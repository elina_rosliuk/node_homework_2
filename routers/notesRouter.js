const express = require('express');
const {
  getAllNotes,
  createNote,
  checkNote,
  getNote,
  updateNote,
  deleteNote,
} = require('../controllers/notesController');
const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateText} = require('./middlewares/validateText');

const router = new express.Router();

router.get('/notes', authMiddleware, asyncWrapper(getAllNotes));
router.get('/notes/:id', authMiddleware, asyncWrapper(getNote));
router.post('/notes', authMiddleware, validateText, asyncWrapper(createNote));
router.patch('/notes/:id', authMiddleware, asyncWrapper(checkNote));
router.put('/notes/:id',
    authMiddleware,
    validateText,
    asyncWrapper(updateNote),
);
router.delete('/notes/:id', authMiddleware, asyncWrapper(deleteNote));

module.exports = router;
