/** Wrapper that additionally checks for errors */
/**
   * @param {function} callback - Function to be executed
   * @return {function} - Returns callback
*/
function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
}

module.exports = {
  asyncWrapper,
};
