const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWTSecret} = require('../config');

module.exports.register = async (req, res) => {
  const {username, password} = req.body;

  const user = new User({
    username,
    password: await bcrypt.hash(password, 9),
  });

  await user.save();

  res.status(200).json({message: 'Success'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (!user) {
    return res.status(400).json({
      message: `No user with '${username}' username was found!`,
    });
  }

  const isPasswordCorrect = await bcrypt.compare(password, user.password);

  if (!isPasswordCorrect) {
    return res.status(400).json({message: 'Wrong password'});
  }

  const JWT_TOKEN = jwt.sign({username: user.id, _id: user.id}, JWTSecret);

  res.header('authorization', JWT_TOKEN);

  res.status(200).json({message: 'Success', jwt_token: JWT_TOKEN});
};
