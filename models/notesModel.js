const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const notesSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: new Date(),
  },
});

module.exports.Note = mongoose.model('Note', notesSchema);
