const {Note} = require('../models/notesModel');

const findNoteById = async (id, userId) => {
  return await Note.findOne({userId, _id: id});
};

module.exports.getAllNotes = async (req, res) => {
  const userCredentials = req.user;
  const {limit = 100, offset = 0} = req.query;

  let notes;

  try {
    notes = await Note.find({userId: userCredentials._id})
        .limit(+limit)
        .skip(+offset);
  } catch (err) {
    return res.status(400).json({message: 'Client error'});
  }

  const dataToShow = notes.map((note) => {
    const {_id, userId, text, completed, createdDate} = note;
    note = {_id, userId, text, completed, createdDate};

    return note;
  });

  res.status(200).json({message: 'Success', notes: dataToShow});
};

module.exports.createNote = async (req, res) => {
  const userCredentials = req.user;
  const {text} = req.body;

  const note = new Note({userId: userCredentials._id, text});

  note.save();

  res.status(200).json({message: 'Success'});
};

module.exports.getNote = async (req, res) => {
  const userCredentialsId = req.user._id;
  const {id} = req.params;

  const note = await findNoteById(id, userCredentialsId);

  if (!note) {
    return res.status(400).json({message: 'Client error'});
  }

  const {_id, userId, text, completed, createdDate} = note;

  res.status(200).json({
    message: 'Success',
    note: {_id, userId, text, completed, createdDate},
  });
};

module.exports.updateNote = async (req, res) => {
  const {text} = req.body;
  const userCredentialsId = req.user._id;
  const {id} = req.params;

  const note = await findNoteById(id, userCredentialsId);

  if (!note) {
    return res.status(400).json({message: 'Client error'});
  }

  await note.updateOne({text});

  note.save();

  res.status(200).json({message: 'Success'});
};

module.exports.checkNote = async (req, res) => {
  const userCredentialsId = req.user._id;
  const {id} = req.params;

  const note = await findNoteById(id, userCredentialsId);

  if (!note) {
    return res.status(400).json({message: 'Client error'});
  }

  const isCompleted = !note.completed;

  await note.updateOne({completed: isCompleted});

  note.save();

  res.status(200).json({message: 'Success'});
};

module.exports.deleteNote = async (req, res) => {
  const {id} = req.params;

  try {
    await Note.remove({_id: id});
  } catch (err) {
    return res.status(400).json({message: 'Client error'});
  }

  res.status(200).json({message: 'Success'});
};
