const express = require('express');
const {asyncWrapper} = require('./helpers');
const {validateRegistration} = require('./middlewares/validateRegistration');
const {register, login} = require('./../controllers/authController');

const router = new express.Router();

router.post(
    '/auth/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(register),
);
router.post('/auth/login', asyncWrapper(login));

module.exports = router;
