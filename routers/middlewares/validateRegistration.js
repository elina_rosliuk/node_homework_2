const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().alphanum().min(2).required(),

    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{5,30}$')),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  next();
};
