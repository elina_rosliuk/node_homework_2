const Joi = require('joi');

module.exports.validateText = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string().min(1),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    return res.status(400).json({message: err.message});
  }

  next();
};
