const {User} = require('./../models/userModel');
const bcrypt = require('bcrypt');

module.exports.getProfile = async (req, res) => {
  const userCredentials = req.user;
  const user = await User.findById({_id: userCredentials._id});

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }

  const {_id, username, createdDate} = user;

  res.status(200).json({user: {_id, username, createdDate}});
};

module.exports.deleteProfile = async (req, res) => {
  const userCredentials = req.user;

  try {
    await User.remove({_id: userCredentials._id});
  } catch (err) {
    return res.status(400).json({message: 'Client error'});
  }

  res.status(200).json({message: 'Success'});
};

module.exports.changePassword = async (req, res) => {
  const userCredentials = req.user;
  const {oldPassword, newPassword} = req.body;

  if (oldPassword === newPassword) {
    return res.status(400).json({
      message: 'New password should be different with old one',
    });
  }

  let user;

  try {
    user = await User.findById({_id: userCredentials._id});
  } catch (err) {
    return res.status(400).json({message: 'Client error'});
  }

  const verified = await bcrypt.compare(oldPassword, user.password);

  if (!verified) {
    return res.status(400).json({message: 'Wrong current password'});
  }

  await user.updateOne({password: await bcrypt.hash(newPassword, 9)});

  user.save();

  res.status(200).json({message: 'Success'});
};
